#coding: utf-8
def calc(hor,ver):
	
	vals={
		"A":1,
		"B":2,
		"C":3,
		"D":4,
		"E":5,
		"F":6,
		"G":7,
		"H":8,
		"a":1,
		"b":2,
		"c":3,
		"d":4,
		"e":5,
		"f":6,
		"g":7,
		"h":8,}

	hor = vals[hor]

	if (((8-hor)>=2) and ((8-hor)<6)) and (((8-ver)>=2) and ((8-ver)<6)):
		moves=8
	elif (((8-hor)>=2) and ((8-hor)<6)) or (((8-ver)>=2) and ((8-ver)<6)):
		moves=6
	if ((((8-hor)==1) or ((8-hor)==6)) and (((8-ver)==1) or ((8-ver)==6))) or ((((8-hor)==0) or ((8-hor)==7)) or (((8-ver)==0) or ((8-ver)==7))):
		moves=4
	if ((((8-hor)==0) or ((8-hor)==7)) and (((8-ver)==0) or ((8-ver)==7))):
		moves=2
	if ((((8-hor)==0) or ((8-hor)==7)) and (((8-ver)==6) or ((8-ver)==1)))or((((8-hor)==1) or ((8-hor)==6)) and (((8-ver)==0) or ((8-ver)==7))):
		moves=3
	
	return moves

print u"""Данная программа определяет сколько вариантов ходов имеет 'конь' на шахматной доске с выбранной позиции."""
print u"""Введите адрес фигуры разделяя индексы пробелом"""
coordinates=raw_input()
position=coordinates.split()
print calc(position[0],int(position[1]))